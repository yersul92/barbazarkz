<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Redirect;

class LanguageController extends Controller
{
    public function index(Request $request, $lng) {
        Session::put('locale',$lng);
        return Redirect::back();
    }
}
