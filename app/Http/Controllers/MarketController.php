<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mobizon\MobizonApi;

class MarketController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function send()
    {
        $resText = '';
        $alphaname = 'TEST';
        $api = $this->getApi();
        if ($api->call('message',
            'sendSMSMessage',
            array(
                'recipient' => '77057174858',
                'text'      => 'Test sms message',
                'from'      => $alphaname, 
            ))
        ) {
            $messageId = $api->getData('messageId');
            $resText = 'Message created with ID:' . $messageId;
            if ($messageId) {
                $resText = $resText.'Get message info...';
                $messageStatuses = $api->call(
                    'message',
                    'getSMSStatus',
                    array(
                        'ids' => array($messageId, '13394', '11345', '4393')
                    ),
                    array(),
                    true
                );
                if ($api->hasData()) {
                    foreach ($api->getData() as $messageInfo) {
                        $resText = $resText.'Message # ' . $messageInfo->id . " status:\t" . $messageInfo->status;
                    }
                }
            }
        } else {
            $resText = 'An error occurred while sending message: [' . $api->getCode() . '] ' . $api->getMessage() . 'See details below:';
            $resText = $resText.var_dump(array($api->getCode(), $api->getData(), $api->getMessage()));
        }
        return response()->json([ 'code'=>200,'text'=>$resText], 200);
    }

    private function getApi(){
        return new MobizonApi('e549137c473771681b81fd0c9b1a395ca8ea30a5');
    }
}
