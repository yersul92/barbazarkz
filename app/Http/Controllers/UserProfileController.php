<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Auth\StatefulGuard;
use App\Services\Mobizon\MobizonClient;
use App\User;
use App\PhoneConfirm;
use Lang;
class UserProfileController extends Controller
{
    protected $mobizonClient;
    //MobizonClientInterface $mobizonClient 
    public function __construct() {
        $this->mobizonClient = new MobizonClient();
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('user_profile.index')->with('pageHeader', Lang::get("dict.user_profile"));
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function stores(Request $request)
    {
        return view('user_profile.stores')->with('pageHeader', Lang::get("dict.my_web_store"));;
    }

    public function updateUserFullName(Request $request, $id) {
        // catching the new comment
        $this->validate($request, [
            'name' => 'required|max:255'            
        ]);
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->save();
        return redirect()->action('UserProfileController@index');
        //$this->index($request,Lang::get("dict.name_change_success"));
        //return view('user_profile.index')->with('pageHeader', Lang::get("dict.user_profile"))->with('success',Lang::get("dict.name_change_success"));
    }

    public function updateUserPassword(Request $request, $id) {
        // catching the new comment
        $this->validate($request, [
            'password' => 'required|string|min:6'
        ]);
        $user = User::find($id);
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect()->action('UserProfileController@index')->with('success',Lang::get("dict.password_change_success"));
    }

    public function sendCode(Request $request) {
        // catching the new comment
        $currentUserId = \Auth::user()->id;
        $currentUserPhone = \Auth::user()->phone;
        $currentUserPhone = trim(str_replace('(','',str_replace(')','',str_replace('-','',$currentUserPhone))));
        $currentUserPhone = '7'.substr($currentUserPhone, 1);
        $randNumber = rand(10000, 99999); 
        
        $phoneConfirm = PhoneConfirm::where('user_id', $currentUserId)->first();
        if(isset($phoneConfirm)){
            $phoneConfirm->code=$randNumber;
            $phoneConfirm->save();
        }else{
            $newPhoneConfirm = PhoneConfirm::create(['user_id' => $currentUserId,'code'=>$randNumber]);
        }
        $this->mobizonClient->send($currentUserPhone,'BarBazar.kz code: '.$randNumber);
        return response()->json([ 'code'=>200], 200);
    }

    public function confirmCode(Request $request) {
        // catching the new comment
        $code = $request->get('code');
        $currentUser = \Auth::user();        
        $phoneConfirm = PhoneConfirm::where('user_id', $currentUser->id)->where('code',$code)->first();
        if(isset($phoneConfirm)&&$phoneConfirm->user_id>0){
            $currentUser->is_phone_active = true;
            $currentUser->save();
            return response()->json([ 'code'=>200], 200);
        }else{
            return response()->json([ 'code'=>404], 404);
        }
        
    }

     /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }


}
