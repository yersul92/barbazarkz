<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneConfirm extends Model
{
    protected $fillable = ['user_id', 'code'];
}
