<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MarketController@index');

Route::get('/send', 'SendMessageController@sendTestMessage');

Route::get('language/{lng}',['as'=>'language',"Middleware"=>"LanguageSwitcher","uses"=>"LanguageController@index"]);

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('test', ['uses' => 'TestController@index']);
    Route::put('test/update/{id}', ['as' => 'test/update', 'uses' => 'TestController@update']);
    Route::post('test/bulk_update', ['as' => 'test/bulk_update', 'uses' => 'TestController@bulk_update']);


	Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
	Route::get('/home', 'HomeController@index');
	Route::get('/user_profile', 'UserProfileController@index');
	Route::get('/user_profile/stores', ['as'=>'user_profile/stores','uses'=>'UserProfileController@stores']);
    Route::put('user_profile/name/{id}', ['as'=>'user_profile/name','uses'=>'UserProfileController@updateUserFullName']); 
	Route::put('user_profile/password/{id}', ['as'=>'user_profile/password','uses'=>'UserProfileController@updateUserPassword']); 
	Route::put('user_profile/phone/{id}', ['as'=>'user_profile/phone','uses'=>'UserProfileController@updateUserPhone']); 
	//Route::put('user_profile/email/{id}', ['as'=>'user_profile/email','uses'=>'UserProfileController@updateUserEmail']); 
    Route::post('user_profile/sendcode', ['as' => 'user_profile/sendcode', 'uses' => 'UserProfileController@sendCode']);
	Route::post('user_profile/confirmcode', ['as' => 'user_profile/confirmcode', 'uses' => 'UserProfileController@confirmCode']);
	Route::resource('users','UserController');

	Route::get('roles',['as'=>'roles.index','uses'=>'RoleController@index','middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
	Route::get('roles/create',['as'=>'roles.create','uses'=>'RoleController@create','middleware' => ['permission:role-create']]);
	Route::post('roles/create',['as'=>'roles.store','uses'=>'RoleController@store','middleware' => ['permission:role-create']]);
	Route::get('roles/{id}',['as'=>'roles.show','uses'=>'RoleController@show']);
	Route::get('roles/{id}/edit',['as'=>'roles.edit','uses'=>'RoleController@edit','middleware' => ['permission:role-edit']]);
	Route::patch('roles/{id}',['as'=>'roles.update','uses'=>'RoleController@update','middleware' => ['permission:role-edit']]);
	Route::delete('roles/{id}',['as'=>'roles.destroy','uses'=>'RoleController@destroy','middleware' => ['permission:role-delete']]);

	Route::get('application',['as'=>'application.index','uses'=>'ApplicationController@index','middleware' => ['permission:item-list|item-create|item-edit|item-delete']]);
	Route::get('application/create',['as'=>'application.create','uses'=>'ApplicationController@create','middleware' => ['permission:item-create']]);
	Route::post('application/create',['as'=>'application.store','uses'=>'ApplicationController@store','middleware' => ['permission:item-create']]);
	Route::get('application/{id}',['as'=>'application.show','uses'=>'ApplicationController@show']);
	Route::get('application/{id}/edit',['as'=>'application.edit','uses'=>'ApplicationController@edit','middleware' => ['permission:item-edit']]);
	Route::patch('application/{id}',['as'=>'application.update','uses'=>'ApplicationController@update','middleware' => ['permission:item-edit']]);
	Route::delete('application/{id}',['as'=>'application.destroy','uses'=>'ApplicationController@destroy','middleware' => ['permission:item-delete']]);
});