<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>BarbazarKZ</title>

		<meta name="description" content="3 styles with inline editable feature" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="dashboard_assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="dashboard_assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="dashboard_assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="dashboard_assets/css/jquery.gritter.min.css" />
		<link rel="stylesheet" href="dashboard_assets/css/select2.min.css" />
		<link rel="stylesheet" href="dashboard_assets/css/bootstrap-datepicker3.min.css" />
		<link rel="stylesheet" href="dashboard_assets/css/bootstrap-editable.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="dashboard_assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="dashboard_assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="dashboard_assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="dashboard_assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="dashboard_assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="dashboard_assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="dashboard_assets/js/ace-extra.min.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="dashboard_assets/js/html5shiv.min.js"></script>
		<script src="dashboard_assets/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default          ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="index.html" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							{{trans('dict.settings')}}
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="purple">
							<a href="/barbazarkz/public/language?locale=kz">
								ҚАЗ
							</a>
						</li>
						<li class="purple">
							<a href="/barbazarkz/public/language?locale=ru">
								РУС
							</a>
						</li>
						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<span class="user-info">
									<small>{{trans('dict.welcome')}},</small>
									{{Auth::user()->name}}
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-shopping-cart"></i>
										{{trans('dict.go_to_market')}}
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="{{ url('/logout') }}">
										<i class="ace-icon fa fa-power-off"></i>
										{{trans('dict.logout')}}
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<ul class="nav nav-list">
					<li class="active">
						<a href="{{ url('/user_profile') }}">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> {{trans('dict.profile')}} </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="{{ url('/favourites') }}">
							<i class="menu-icon fa fa-star"></i>
							<span class="menu-text"> {{trans('dict.favourite')}} </span>
						</a>

						<b class="arrow"></b>
					</li>
					@if(Auth::user()->has_store||Auth::user()->is_phone_active)
						<li class="">
							<a href="{{route('user_profile/stores')}}">
								<i class="menu-icon fa fa-briefcase"></i>
								<span class="menu-text">{{trans('dict.my_web_store')}}</span>
							</a>

							<b class="arrow"></b>
						</li>
					@endif
					<!--li class="">
						<a href="index.html">
							<i class="menu-icon fa fa-cogs"></i>
							<span class="menu-text"> Dictionaries </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="index.html">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> Workers </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="index.html">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> Applications/ Applications for worker </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="index.html">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> All Boutique Owners / All Boutique Owners for worker</span>
						</a>

						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="index.html">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> All Boutiques/ All Boutiques for worker </span>
						</a>

						<b class="arrow"></b>
					</li-->				

				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">More Pages</a>
							</li>
							<li class="active">User Profile</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">						
						<div class="page-header">
							<h1>
								{{$pageHeader}}
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								@yield('content')
								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Ace</span>
							Application &copy; 2013-2014
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="dashboard_assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			$.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
				var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

				if (token) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
				}
			});
			if('ontouchstart' in document.documentElement) document.write("<script src='dashboard_assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			jQuery(function($) {
		
					//editables on first profile page
					if($.fn&&$.fn.editable){
						$.fn.editable.defaults.mode = 'inline';
						$.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
						$.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>'+
													'<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';    
					
					}
			});
			$('.editable-custom .edit-button-custom').on('click',function(evt){
			//	$(evt.target.parentElement.parentElement).addClass('hidden');
			//	$(evt.target.parentElement.parentElement.parentElement).find('.edit-form-custom').removeClass('hidden');
			    var hideDataTarget = this.getAttribute('data-target-hide');
				if(hideDataTarget){
					if($('#'+hideDataTarget)[0]){
						$('#'+hideDataTarget).addClass('hidden');
					}
				}
				var showDataTarget = this.getAttribute('data-target-show');
				if(showDataTarget){
					if($('#'+showDataTarget)[0]){
						$('#'+showDataTarget).removeClass('hidden');
					}
				}
			});
            


		</script>
		<script src="dashboard_assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="dashboard_assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="dashboard_assets/js/jquery-ui.custom.min.js"></script>
		<script src="dashboard_assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="dashboard_assets/js/jquery.gritter.min.js"></script>
		<script src="dashboard_assets/js/bootbox.js"></script>
		<!--script src="dashboard_assets/js/bootstrap-datepicker.min.js"></script-->
		<script src="dashboard_assets/js/jquery.hotkeys.index.min.js"></script>
		<script src="dashboard_assets/js/bootstrap-wysiwyg.min.js"></script>
		<!--script src="dashboard_assets/js/select2.min.js"></script-->
		<!--script src="dashboard_assets/js/spinbox.min.js"></script-->
		<!--script src="dashboard_assets/js/bootstrap-editable.min.js"></script>
		<script src="dashboard_assets/js/ace-editable.min.js"></script-->
		<script src="dashboard_assets/js/jquery.maskedinput.min.js"></script>

		<!-- ace scripts -->
		<script src="dashboard_assets/js/ace-elements.min.js"></script>
		<script src="dashboard_assets/js/ace.min.js"></script>

		@yield('scripts') 
	</body>
</html>
