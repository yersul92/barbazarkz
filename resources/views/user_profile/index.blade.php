@extends('layouts.dashboard')
 
@section('content')
    @if(isset($success))
        <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <p>
                <strong>
                    <i class="ace-icon fa fa-check"></i>
                    {{trans('dict.congrats')}}
                </strong>
                {{$success}}
            </p>
        </div> 
    @endif
    @if ($errors->has('name'))
       <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <strong>
                {{ $errors->first('name') }}
            </strong>
            <br>
        </div> 
    @endif
    @if ($errors->has('password'))
       <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <strong>
                {{ $errors->first('password') }}
            </strong>
            <br>
        </div> 
    @endif     
<!-- PAGE CONTENT BEGINS -->
    @if(Auth::user()->has_store==true)
        <div class="clearfix">
            <div class="pull-left alert alert-success no-margin alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>

                <i class="ace-icon fa fa-umbrella bigger-120 blue"></i>
                {{Auth::user()->pay_date}} {{trans('dict.pay_days_left')}}
            </div>
        </div>
    @else
        <div class="hidden">{{$disableButton = ''}} </div>
        @if (Auth::user()->is_phone_active==false)
        <div class="hidden"> {{$disableButton = 'disabled'}}</div>
        <div class="alert alert-danger">
                <strong>
                    {{ trans('dict.confirm_phone_warning') }}
                </strong>
                <br>
            </div> 
        @endif
        <div class="col-xs-12 col-sm-3">
            <a href="#" class="btn btn-primary" data-toggle="button" {{$disableButton}}>{{trans('dict.create_store')}}</a>
        </div>	
    @endif
    <div id="user-profile-1" class="user-profile row">
            <div class="col-xs-12 col-sm-9">											
                <div class="profile-user-info profile-user-info-striped">
                    <!--div class="profile-info-row">
                        <div class="profile-info-name"> trans('dict.login') </div>

                        <div class="profile-info-value">
                            <span>{{Auth::user()->login}}</span>
                        </div>
                    </div-->
                    
                    <div class="profile-info-row">
                        <div class="profile-info-name"> {{trans('dict.name')}} </div>

                        <div class="profile-info-value">
                            <div class="editable-custom">
                                <div id="edit-name-info-target">
                                    <span class="" id="username">{{Auth::user()->name}}</span>
                                    <button class="btn btn-info btn-sm edit-button-custom" data-target-hide="edit-name-info-target" data-target-show="edit-name-form-target">
                                        <i class="ace-icon fa fa-pencil"></i>
                                    </button>
                                </div> 
                                <div class='hidden' id="edit-name-form-target">
                                    <form class="input-group" method="POST" action="{{route('user_profile/name', ['id'=>Auth::user()->id])}}">
                                        {{ csrf_field() }}
                                        <input name="_method" type="hidden" value="PUT">
                                        <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <span class="ace-icon fa fa-check icon-on-right bigger-110"></span>
                                                {{trans('dict.save')}}
                                            </button>
                                        </span>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-sm edit-button-custom" data-target-show="edit-name-info-target" data-target-hide="edit-name-form-target">
                                                <span class="ace-icon fa fa-times icon-on-right bigger-110"></span>
                                                {{trans('dict.cancel')}}
                                            </button>
                                        </span>

                                    </form>
                                </div>   
                            </div>
                            
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> {{trans('dict.phone')}} <small class="text-warning">8 (XXX) XXX-XXXX</small></div>

                        <div class="profile-info-value">
                            <span class="editable" id="phone">{{isset(Auth::user()->phone)?Auth::user()->phone:trans('dict.not_defined')}}</span>
                            @if (Auth::user()->is_phone_active==false)
                                <a href="#modal-form" class="btn btn-primary" data-toggle="modal" >{{trans('dict.confirm_phone')}}</a>

                                <div id="modal-form" class="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">{{trans('dict.confirm_phone_warning')}}</h4>
											</div>

											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12 col-sm-12">
														<div class="form-group">
															<label for="form-field-select-3">{{trans('dict.confirm_instruction_text')}}</label>
														</div>

														<div class="space-4"></div>

														<div class="form-group">
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>{{trans('dict.confirm_code')}}</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-9">
                                                                 <input type="text" id="confirm-phone-code" placeholder="{{trans('dict.confirm_code')}}" />
                                                            </div>
														</div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
                                                <div class="alert alert-danger hidden" id="bad-code-error">
                                                    <button type="button" class="close" data-dismiss="alert">
                                                        <i class="ace-icon fa fa-times"></i>
                                                    </button>
                                                    {{trans('dict.confirm_bad_code_error')}}
                                                    <br>
                                                </div>
                                                <div class="alert alert-danger  hidden" id="confirm-error">
                                                    <button type="button" class="close" data-dismiss="alert">
                                                        <i class="ace-icon fa fa-times"></i>
                                                    </button>
                                                    {{trans('dict.confirm_error')}}
                                                    <br>
                                                </div>
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													{{trans('dict.cancel')}}
												</button>

												<button class="btn btn-sm btn-primary" id="btn-submit-code-confirm">
													<!--i class="ace-icon fa fa-check "></i-->
                                                    <i class="ace-icon fa fa-check" id="confirm-code-loader"></i> 
													{{trans('dict.save')}}
												</button>
											</div>
										</div>
									</div>
								</div>
                            @endif
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> {{trans('dict.password')}} </div>
                        <div class="profile-info-value">
                 
                            <div class="editable-custom">
                                <div id="edit-password-info-target">
                                    <input type="password" value="password" id="password" readonly/>
                                    <button class="btn btn-info btn-sm edit-button-custom" data-target-hide="edit-password-info-target" data-target-show="edit-password-form-target">
                                        <i class="ace-icon fa fa-pencil"></i>
                                    </button>
                                </div> 
                                <div id='edit-password-form-target' class=" hidden">
                                    <form class="input-group" method="POST" action="{{route('user_profile/password', ['id'=>Auth::user()->id])}}">
                                        {{ csrf_field() }}
                                        <input name="_method" type="hidden" value="PUT">
                                        <input type="text" class="form-control" name="password" value="password"/>
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <span class="ace-icon fa fa-check icon-on-right bigger-110"></span>
                                                {{trans('dict.save')}}
                                            </button>
                                        </span>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-sm edit-button-custom" data-target-show="edit-password-info-target" data-target-hide="edit-password-form-target">
                                                <span class="ace-icon fa fa-times icon-on-right bigger-110"></span>
                                                {{trans('dict.cancel')}}
                                            </button>
                                        </span>
                                    </form>
                                </div>   
                            </div>    
                        </div>   
                    </div>

                    
                    
                    <!--div class="profile-info-row">
                        <div class="profile-info-name"> trans('dict.email') </div>

                        <div class="profile-info-value">
                            <span class="editable" id="email">isset(Auth::user()->email)?Auth::user()->email:trans('dict.not_defined')</span>
                        </div>
                    </div-->

                    <div class="profile-info-row">
                        <div class="profile-info-name"> {{trans('dict.register_date')}} </div>

                        <div class="profile-info-value">
                            <span>{{Auth::user()->created_at}}</span>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
    <!-- PAGE CONTENT ENDS -->
@endsection
@section('scripts')
    <!-- inline scripts related to this page -->
		<script type="text/javascript">
			$(document).ready(function(){
            	//$.ajaxSetup({
                //    headers: {
                //        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //    }
                //});
				//editables 
				
				//text editable
			    /*
                $('#username')
				.editable({
					type: 'text',
                    url:'route('user_profile/name', ['id'=>Auth::user()->id])',
                    send:'always',
                    validate:function(text){
                        //alert(text+" validation");
                    },
                    success: function(response, newValue) {
					//alert('success'+response.status);
					},
                    ajaxOptions: {
                        type: 'put',
                    }
			    });
           
                $('#password')
				.editable({
					type: 'password',
                    url:'route('user_profile/password', ['id'=>Auth::user()->id])',
                    send:'always',
                    validate:function(text){
                        //alert(text+" validation");
                    },
                    success: function(response, newValue) {
					//alert('success'+response.status);
					},
                    ajaxOptions: {
                        type: 'put',
                    }
			    });
                */   
                if($("#btn-submit-code-confirm")[0]){
                    $("#btn-submit-code-confirm").on('click',function(){
                        var code = $('#confirm-phone-code').val();
                        $('#confirm-code-loader').removeClass('fa-check').addClass('fa-spinner fa-spin white bigger-175');
                        $.ajax({
                            method: "POST",
                            data:{code:code},
                            url: "{{route('user_profile/confirmcode')}}",
                            success:function(data){ 
                                console.log('success');
                                //console.log(data);
                                if(data.code===200){
                                      
                                }else{
                                   $('#confirm-code-loader').removeClass('fa-spinner fa-spin white bigger-175').addClass('fa-check');
                                   $('#bad-code-error').removeClass('hidden');
                                }
                                
                            },
                            error:function(data){
                                console.log('error');
                                console.log(data);
                                $('#confirm-error').removeClass('hidden');
                                $('#confirm-code-loader').removeClass('fa-spinner fa-spin white bigger-175').addClass('fa-check');
                            },
                         })
                    });
                }

                $('#confirm-phone-code').on('keyup',(function() {
                    if($(this).val() === '') {
                        $("#btn-submit-code-confirm").prop("disabled", true);
                    } else {
                        $('#btn-submit-code-confirm').removeAttr('disabled');
                    }
                }));

                $('#modal-form').on('shown.bs.modal', function () {
                        $.ajax({
                            method: "POST",
                            url: "{{route('user_profile/sendcode')}}",
                            success:function(data){ 
                                console.log('success');
                                console.log(data);
                            },
                            error:function(data){
                                console.log('error');
                                console.log(data);
                            },
                         })
                });    

                $('#modal-form').on('hidden.bs.modal', function () {
                        $('#confirm-phone-code').val('');
                        $("#btn-submit-code-confirm").prop("disabled", true);
                        $('#bad-code-error').addClass('hidden');
                        $('#confirm-error').addClass('hidden');
                });    

                $.mask.definitions['~']='[+-]';
				$('.input-mask-phone').mask('8(999) 999-9999');
			
				/////////////////////////////////////
				$(document).one('ajaxloadstart.page', function(e) {
					//in ajax mode, remove remaining elements before leaving page
					try {
						$('.editable').editable('destroy');
					} catch(e) {}
					$('[class*=select2]').remove();
				});
			});
		</script>
@endsection