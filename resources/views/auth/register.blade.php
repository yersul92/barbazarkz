@extends('layouts.market')

@section('scripts')
<script src="dashboard_assets/js/jquery.maskedinput.min.js"></script>
@endsection
@section('scripts-document-ready')
    $.mask.definitions['~']='[+-]';
	$('.input-mask-phone').mask('9-(999)-999-99-99');
@endsection
@section('content')

<!-- sign up-page -->
<div class="login-page">
    <div class="container"> 
        <div class="login-body">
            <form action="#" method="post">
                {{ csrf_field() }}

                <input id="name" type="text"  class="user" name="name" value="{{ old('name') }}" placeholder="{{ trans('dict.name') }}" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif                            

                <input id="phone" type="text" class="user input-mask-phone" name="phone"  placeholder="{{trans('dict.phone').' 9-(999)-999-99-99'}}" value="{{ old('phone') }}" required>
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif   
                
                <input id="password" type="password" class="lock" name="password" placeholder="{{ trans('dict.password') }}" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

                <input id="password-confirm" type="password" class="lock" placeholder="{{ trans('dict.confirm_password') }}" name="password_confirmation" required>


                <input type="submit" value="{{ trans('dict.register') }} ">
            </form>
        </div>  
        <h6>{{trans('dict.already_have_an_account')}}? <a href="{{ url('/login') }}">{{trans('dict.login')}} »</a> </h6>  
    </div>
</div>
<!-- //sign up-page --> 
@endsection