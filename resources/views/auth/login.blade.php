@extends('layouts.market')

@section('scripts')
<script src="dashboard_assets/js/jquery.maskedinput.min.js"></script>
@endsection
@section('scripts-document-ready')
    $.mask.definitions['~']='[+-]';
	$('.input-mask-phone').mask('9-(999)-999-99-99');
@endsection
@section('content')

<!-- login-page -->
	<div class="login-page">
		<div class="container"> 
			<h3 class="w3ls-title w3ls-title1">Login to your account</h3>  
			<div class="login-body">
                <form method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
					<strong>{{trans('dict.phone').' (999) 999-99-99'}}</strong>
                    <input id="phone" type="text" class="user input-mask-phone" name="phone"  placeholder="{{trans('dict.phone').' (999) 999-99-99'}}" value="{{ old('phone') }}" required>
                    @if ($errors->has('phone'))
                        <span class="help-block" style="color:red;">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
					<strong>{{trans('dict.password')}}</strong>
					<input id="password" type="password" class="lock" name="password" placeholder="{{trans('dict.password')}}" required>
                    @if ($errors->has('password'))
                        <span class="help-block" style="color:red;">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
					<input type="submit" value="Login">
					<div class="forgot-grid">
						<label class="checkbox"><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><i></i>Remember me</label>
                        
						<div class="forgot">
							<a href="{{ route('password.request') }}">Forgot Password?</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</form>
			</div>  
			<h6> Not a Member? <a href="{{ url('/register') }}">Sign Up Now »</a> </h6> 
			<div class="login-page-bottom social-icons">
				<h5>Recover your social account</h5>
				<ul>
					<li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
					<li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
					<li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
					<li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
					<li><a href="#" class="fa fa-rss icon rss"> </a></li> 
				</ul> 
			</div>
		</div>
	</div>
	<!-- //login-page --> 
    @endsection