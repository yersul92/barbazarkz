<?php

return [
    "settings"=>"Настройки",
    "email"=>"Электронный адрес",
    "name"=>"Полное имя",
    "login"=>"Войти",
    "password"=>"Пароль",
    "confirm_password"=>"Подтвердить пароль",
    "phone"=>"Номер телефона",
    "register"=>"Регистрация",
    "welcome"=>"Добро пожаловать",
    "go_to_market"=>"Зайти на базар",
    "logout"=>"Выход",
    "profile"=>"Профиль",
    "favourite"=>"Таңдаулылар",
    "user_profile"=>"Данные о пользователе",
    "register_date"=>"Время регистрации",
    "pay_days_left"=>"Электронная площадка будет не достпен через %d дней. Не забудьте заплатить!",
    "create_store"=>"Открыть электронную площадку",
    "not_defined"=>"Вводить",
    "save"=>"Сохранить",
    "name_change_success"=>"Имя изменено успешно!",
    "password_change_success"=>"Пароль изменен успешно!",
    "congrats"=>"Поздравляем!",
    "cancel"=>"Отмена",
    "already_have_an_account"=>"Если вы зарегистрированы",
    "all_markets_here"=>"Все базары у нас!",
    "market_top_header_quote"=>"У нас торговаться легко | Откройте электронную площадку | Покупайте",
    "my_account"=>"Мой аккаунт"
];