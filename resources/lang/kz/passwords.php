<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Құпия сөз кем дегенде 6 таңбадан тұруы керек және құпия сөзді қайта терілу керек.',
    'reset' => 'Құпия сөз сәтті өзгертілді!',
    'sent' => 'Электронды поштаңызға құпия сөзді өзгерту туралы хат жіберілді, тексеріңіз!',
    'token' => 'Бұл кұпия сөзді өзгерту token-і қате.',
    'user' => "Мұндай электронды поштамен қолданушы тіркелмеген",

];
