<?php

return [
    "settings"=>"Settings",
    "email"=>"Email",
    "name"=>"Full name",
    "login"=>"User name",
    "password"=>"Password",
    "confirm_password"=>"Confirm password",
    "phone"=>"Phone number"
];